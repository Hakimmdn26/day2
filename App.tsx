import React, { useEffect, useState } from 'react';
import { SafeAreaView, StyleSheet, Text, View, FlatList, ScrollView, Image } from 'react-native';
import icon from './src/Image/download.png'

const App = () => {
    const [item, setItem] = useState(null)


const dataHistory = async() => {

    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    
    var raw = '';
    
    var requestOptions = {
      method: 'GET',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };
    
    fetch("https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/transaction.json", requestOptions)
      .then(response => response.text())
      .then(result => {
          const val = JSON.parse(result);
          setItem(Object.entries(val));
        // console.log(result);
    })
      .catch(error => console.log('error', error));
}
useEffect(() => {
    dataHistory()
  }, [])

return (
<SafeAreaView style={styles.container}>
    <View style={styles.header}>
        <Text style={styles.headerText}>History Pembelian</Text>
        
    </View>
    <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.content}>
            {item?.map((e:any,index:any)=>{
                return(
                    <View style = {{
                        flexDirection : 'row',
                        borderWidth :1, 
                        margin : 10,
                        borderRadius : 7,
                        padding :7,
                        backgroundColor : '#e6f2ff',
                        justifyContent : 'space-between',
                        borderColor:'grey',
                        borderBottomWidth: 4,
                        borderRightWidth:4,
                        shadowColor: 'grey',
                        shadowOffset: { width: 0, height: 2 },
                        shadowOpacity: 0.9,
                        shadowRadius: 3,
                        elevation: 3,
                        }}>
                    
                        <View>
                            <View style={{
                                flexDirection:'row',
                                }}>
                                <Text style={{
                                    fontWeight:'bold',
                                    fontFamily :'TitilliumWeb-Black'
                                    
                                }}>Pengirim : </Text>
                                <Text style={{
                                    fontFamily :'TitilliumWeb-Black'
                                }}>{e[0]}</Text>
                            </View>
                            <View style={{
                                flexDirection:'row',
                                }}>
                                <Text style={{
                                    fontWeight:'bold',
                                    fontFamily :'TitilliumWeb-Black'
                                }}>Penerima : </Text>
                                <Text style={{
                                    fontFamily :'TitilliumWeb-Black'
                                }}>{e[1].target}</Text>
                            </View>
                            <View style={{
                                flexDirection:'row',
                                }}>
                                <Text style={{
                                    fontWeight:'bold',
                                    fontFamily :'TitilliumWeb-Black'
                                }}>Jenis Transaksi : </Text>
                                <Text style={{
                                    fontFamily :'TitilliumWeb-Black'
                                }}>{e[1].type}</Text>
                            </View>
                            <View style={{
                                flexDirection:'row',
                                }}>
                                <Text style={{
                                    fontWeight:'bold',
                                    fontFamily :'TitilliumWeb-Black'
                                }}>Nominal : </Text>
                                <Text style={{
                                    fontFamily :'TitilliumWeb-Black'
                                }}>{e[1].amount}</Text>
                            </View>
                        </View>
                            <Image 
                                source={icon}
                                style = {{width : 48, height: 48, marginTop : 14}}
                             />
                    </View>
                )
            })

            }
        </View>
    </ScrollView>
</SafeAreaView>
            );
        }; 
        
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#cceeff',
        },
            header: {
                    height: 50,
                    justifyContent: 'center',
                    alignItems: 'center',
                    // borderBottomWidth: 1,
                    // borderBottomColor: '#ccc',
                    backgroundColor: '#008ae6',
                    borderRadius:80,
                    marginTop : 10,
                    marginBottom:20
                },
                headerText: {
                    fontSize: 18,
                    fontWeight: 'bold',
                },
                content: {
                    // flex: 1,
                    // padding: 10,
                    // shadowColor:'#002080',
                    // shadowOffset:{width:2, height:8},
                    // shadowOpacity:0.8,
                    // shadowRadius:3,
                    // elevation:2,
                    
                },
                item: {
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    paddingVertical: 20,
                    borderWidth:0,
                    margin:10,
                    borderRadius:10,
                    padding:5,
                    
                },
                itemText: {
                    fontSize: 16
              
                },
            });
            
export default App;